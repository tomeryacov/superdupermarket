﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using SuperDuperMarket.Models;

namespace SuperDuperMarket.ViewComponents
{
    public class CategoriesViewComponent: ViewComponent
    {
        private SDMDbContext _context;

        public CategoriesViewComponent(SDMDbContext context)
        {
            _context = context;
        }

        public  IViewComponentResult Invoke()
        {
            // TODO : refactor to distinct val
            var categoris = _context.Products.GroupBy(p => p.Category)
                .Select(x => x.FirstOrDefault().Category);

            return View(categoris);
        }

    }


}

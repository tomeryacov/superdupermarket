﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using SuperDuperMarket.Models;

namespace SuperDuperMarket.ViewComponents
{
    public class FeaturedProductsViewComponent : ViewComponent
    {
        private SDMDbContext _context;

        public FeaturedProductsViewComponent(SDMDbContext context)
        {
            _context = context;
        }

        public IViewComponentResult Invoke()
        {
            var products = _context.Products.Take(3);
            return View(products);
        }
    }
}
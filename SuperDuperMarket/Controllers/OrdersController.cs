﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Dynamic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using SuperDuperMarket.Models;


// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace SuperDuperMarket.Controllers
{
    [Authorize]
    public class OrdersController : BaseController
    {
        public OrdersController(SDMDbContext context) : base(context)
        {

        }


        [Authorize(Roles = "Admin, Customer")]
        public IActionResult Index(int? id, string searchString,
             double searchMinPrice = 0, double searchMaxPrice = 9999999,
             double searchMinQuantity = 0, double searchMaxQuantity = 9999999)
        {
             if (searchMaxQuantity == 0) searchMaxQuantity = double.MaxValue;
            if (searchMaxPrice == 0) searchMaxPrice = double.MaxValue;
            int sid = int.Parse(User.Claims
                            .Where(c => c.Type == ClaimTypes.Sid)
                            .Select(c => c.Value).SingleOrDefault());
            // Checking if other user want to see orders that are not his
            if (((id == null) && (!User.IsInRole("Admin")))
                || ((id != sid) && (!User.IsInRole("Admin"))))
            {
                return Redirect("/error/denied");
            }


            IQueryable<Order> orders = _context.Orders.Include(o => o.OrderProduct).ThenInclude(orp => orp.Product);
            if (!String.IsNullOrEmpty(searchString))
                orders = orders.Where(s => s.Branch.Name.Contains(searchString));
            if (id != null)
                orders = orders.Where(o => o.CustomerId == id);
            orders = orders.Where(order =>
                isBetween(searchMinPrice, searchMaxPrice, order.OrderProduct.Sum(orp => orp.Product.Price * orp.Quantity))
                && isBetween(searchMinQuantity, searchMaxQuantity, order.OrderProduct.Sum(orp => orp.Quantity)));
            var query = orders.Join(_context.OrderProducts,
                o => o.Id,
                (op) => op.OrderId,
                 (o, op) =>
                 new
                 {
                     OrdererName = o.Customer.FirstName + " " + o.Customer.LastName,
                     Branch = o.Branch.Name,
                     OrderID = o.Id,
                     OrderProd = op,
                     Time = o.CreationDate
                 });

            var groupBy = query.GroupBy(o => o.OrderID);
            var data = new List<Tuple<String, int, List<Dictionary<String, String>>, string, double, DateTime>>();
            foreach (var group in groupBy)
            {
                var order = group.First();
                
                var products = group.Select(p => p).Join(_context.Products,
                     op => op.OrderProd.ProductId,
                      p => p.Id,
                      (op, p) =>
                      new Dictionary<String, String>
                      {
                          { "Quantity",op.OrderProd.Quantity.ToString()},
                          { "Image" , p.ImagePath },
                          { "Name" , p.Name },
                          { "Price" , p.Price.ToString()}
                      }).ToList();

                double totalPrice = 0;
                int totalQuantity = 0;
                foreach (var product in products)
                {
                    totalPrice += double.Parse(product["Price"]) * int.Parse(product["Quantity"]);
                }
                                
                    data.Add(Tuple.Create(order.OrdererName, order.OrderID, products, order.Branch, totalPrice, order.Time));
            }
            
            return View(data);
        }

        public static bool isBetween (double min, double max, double val)
        {
            return (val <= max && val >= min);
        }
    }
}
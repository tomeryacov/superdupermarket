﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using SuperDuperMarket.Models;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace SuperDuperMarket.Controllers
{
    [Route("api/additionalservice")]
    [ApiController]
    public class AdditionalServiceController : BaseController
    {
        public AdditionalServiceController(SDMDbContext context):base(context)
        {
            Facebook = new FacebookModel();
            Weather = new WeatherModel();

        }

        public GeoLocationModel GeoLocation { get; set; }
        public FacebookModel Facebook { get; set; }
        public WeatherModel Weather { get; set; }

        [HttpGet("geolocation/getgeo")]
        public async Task<ActionResult<Dictionary<string, object>>> GetGeo(string street, string city)
        {
            return GeoLocation.GetGeoLocationByAddress(street,city);
        }

        [HttpGet("weather/getweather")]
        public async Task<ActionResult<double>> GetWeather(double lon, double lat)
        {
            return Weather.GetWeatherByGeo(lon, lat);
        }

        [HttpGet("facebook/getposts")]
        public async Task<ActionResult<String>> GetPosts()
        {
            return  Facebook.GetPosts();
        }
    }
}

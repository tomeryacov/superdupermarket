﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using SuperDuperMarket.Models;

namespace SuperDuperMarket.Controllers
{
    public class ProductsController : BaseController
    {
        private static Random rnd = new Random();
        public ProductsController(SDMDbContext context) : base(context)
        {
        }

        // GET: Products
        public async Task<IActionResult> Index(string searchName, string searchCategory,
            double searchMinPrice = 0, double searchMaxPrice = 9999999)
        {
            if (searchMaxPrice == 0) searchMaxPrice = int.MaxValue;
            List<Product> products = filter(searchName, searchCategory, searchMinPrice, searchMaxPrice, _context.Products).ToList();

            List<Product> recommendedProducts = new List<Product>();
            string category = "";

            if (User.Identity.IsAuthenticated)
            {
                int customerId = int.Parse(User.Claims.FirstOrDefault(claim => claim.Type == ClaimTypes.Sid).Value);
                try
                {
                    var recentOrders = _context.Orders.Where(order => order.CustomerId == customerId).AsQueryable();
                    if (recentOrders.ToList().Count > 3) recentOrders = recentOrders.OrderByDescending(a => a.CreationDate).Take(3);
                    category = (from o in recentOrders
                                join orp in _context.OrderProducts on o.Id equals orp.OrderId
                                join p in _context.Products on orp.ProductId equals p.Id
                                where o.CustomerId == customerId
                                select new
                                {
                                    p.Category,
                                    orp.Quantity
                                }).
                                GroupBy(t => t.Category).
                                Select(t => new { category = t.Key, sum = t.Sum(a => a.Quantity) }).
                                OrderByDescending(t => t.sum).
                                First().category;

                    List<Product> categoryProducts = _context.Products.Where(p => p.Category == category).ToList();
                    for (int i = 0; i < 3 && categoryProducts.Count > 0; i++)
                    {
                        int index = rnd.Next(categoryProducts.Count);
                        recommendedProducts.Add(categoryProducts[index]);
                        categoryProducts.RemoveAt(index);
                    }
                }
                catch (Exception e)
                {
                    Console.WriteLine(e);
                }

                products.RemoveAll(p => recommendedProducts.Where(p2 => p.Id == p2.Id).ToList().Count != 0);
            }

            products.ForEach(p =>
            {
                if (p.Description != null && p.Description.Length > 55) p.Description = p.Description.Substring(0, 55) + "...";
            });
            recommendedProducts.ForEach(p =>
            {
                if (p.Description != null && p.Description.Length > 55) p.Description = p.Description.Substring(0, 55) + "...";
            });
            return View(new ProductsViewModel(products, recommendedProducts, category));
      }

        private IQueryable<Product> filter(string searchName, string searchCategory, double searchMinPrice, double searchMaxPrice, IQueryable<Product> products)
        {
            // In case the user search by Product Name 
            if (!String.IsNullOrEmpty(searchName))
            {
                products = products.Where(p => p.Name.Contains(searchName));
            }
            // In case the user search by Category
            if (!String.IsNullOrEmpty(searchCategory))
            {
                products = products.Where(p => p.Category.Contains(searchCategory));
            }

            // In case the user search by Product Price
            products = products.Where(p =>
            p.Price <= searchMaxPrice &&
            p.Price >= searchMinPrice);

            return products;
        }

        // GET: Products/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var product = await _context.Products
                .FirstOrDefaultAsync(m => m.Id == id);
            if (product == null)
            {
                return NotFound();
            }

            return View(product);
        }

        // GET: Products/Create
        [Authorize(Roles = "Admin")]
        public IActionResult Create()
        {
            return View();
        }

        // POST: Products/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> Create([Bind("Id,Name,Description,Price,Category,ImagePath")] Product product)
        {
            if (ModelState.IsValid)
            {
                if (product.Description == null) product.Description = "";
                if (product.Category == null) product.Category = "Default";
                _context.Add(product);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(product);
        }

        // GET: Products/Edit/5
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var product = await _context.Products.FindAsync(id);
            if (product == null)
            {
                return NotFound();
            }
            return View(product);
        }

        // POST: Products/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> Edit(int id, [Bind("Id,Name,Description,Price,Category,ImagePath")] Product product)
        {
            if (id != product.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(product);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!ProductExists(product.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(product);
        }

        // GET: Products/Delete/5
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var product = await _context.Products
                .FirstOrDefaultAsync(m => m.Id == id);
            if (product == null)
            {
                return NotFound();
            }

            return View(product);
        }

        // POST: Products/Delete/5
        [HttpPost, ActionName("Delete")]
        [Authorize(Roles = "Admin")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var product = await _context.Products.FindAsync(id);
            _context.Products.Remove(product);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool ProductExists(int id)
        {
            return _context.Products.Any(e => e.Id == id);
        }
    }
}

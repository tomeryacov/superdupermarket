﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using SuperDuperMarket.Models;

namespace SuperDuperMarket.Controllers
{
    [Route("api/stats")]
    [ApiController]
    [Authorize(Roles = "Admin")]
    public class StatisticsAPIController : BaseController
    {
        public StatisticsAPIController(SDMDbContext context) : base(context)
        {
        }

        [HttpGet("categories")]
        public async Task<ActionResult<Dictionary<string, int>>> ProductsCategories()
        {
            return _context.Products.GroupBy(p => p.Category)
                .Select(a => new { name = a.Key, count = a.Count() })
                .ToDictionary(g => g.name, g => g.count);
        }

        [HttpGet("prices")]
        public async Task<ActionResult<Dictionary<string, double>>> ProductPrices()
        {
            return _context.Products
                .OrderBy(g => g.Price)
                .ToDictionary(g => g.Name, g => g.Price);
        }

        [HttpGet("categoryprices")]
        public async Task<ActionResult<Dictionary<string, double>>> CategoryAveragePrices()
        {
            return _context.Products.GroupBy(p => p.Category).
                Select(a => new { name = a.Key, avg = a.Average(p => p.Price) }).
                OrderBy(a => a.avg).
                ToDictionary(g => g.name, g => g.avg);
        }

        [HttpGet("productorders")]
        public async Task<ActionResult<Dictionary<string, int>>> ProductsOrders()
        {
            return _context.OrderProducts.Include(op => op.Product).
                GroupBy(op => op.Product).
                Select(g => new { name = g.Key.Name, count = g.Sum(op => op.Quantity) }).
                OrderBy(a => a.count).
                ToDictionary(a => a.name, a => a.count);
        }

        [HttpGet("branchesorders")]
        public async Task<ActionResult<Dictionary<string, int>>> BranchesOrders()
        {
            return _context.OrderProducts.Include(op =>op.Order).Include(op=>op.Order.Branch).
                GroupBy(op => op.Order.Branch).
                Select(g => new { name = g.Key.Name, count = g.Sum(op => op.Quantity) }).
                OrderBy(a => a.count).
                ToDictionary(a => a.name, a => a.count);
        }
        [HttpGet("ordersperday")]
        public async Task<ActionResult<Dictionary<string, int>>> OrdersPerDay()
        {
            return _context.Orders.
                OrderByDescending(o => o.CreationDate).
                GroupBy(o => o.CreationDate.ToShortDateString()).
                Select(o => new
                {
                    date = o.Key,
                    count = o.Count()
                }).ToDictionary(a => a.date, a => a.count);
        }
    }
}
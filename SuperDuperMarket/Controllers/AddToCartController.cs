﻿    using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using SuperDuperMarket.Models;
using Microsoft.EntityFrameworkCore;


namespace SuperDuperMarket.Controllers
{
    [Authorize]
    public class AddToCartController : BaseController
    {
        public AddToCartController(SDMDbContext context) : base(context)
        {

        }


        public IActionResult Index()
        {
            return View(new Tuple<Dictionary<String, Tuple<Product, int>>, List<Branch>>(ExtractCookies(Request.Cookies), _context.Branches.ToList()));
        }

        private Dictionary<String, Tuple<Product, int>> ExtractCookies(IRequestCookieCollection Cookies)
        {
            var registry = Request.Cookies["SDregistery"];

            Dictionary<String, Tuple<Product, int>> dic = new Dictionary<String, Tuple<Product, int>>();
            if (registry != null && registry != "")
            {
                var arr = registry.Split('~');
                if (arr != null && arr.Length != 0)
                {
                    foreach (var key in arr)
                    {
                        Product p = _context.Products.Where(x => x.Id == int.Parse(key))
                            .FirstOrDefault();
                        dic.Add(key, Tuple.Create<Product, int>(p, int.Parse(Request.Cookies["SDSC" + key])));
                    }
                }
            }
            return dic;
        }


        [HttpPost]
        public IActionResult Order(int branchId)
        {
            Dictionary<String, Tuple<Product, int>> dic = ExtractCookies(Request.Cookies);
            if (dic.Count != 0)
            {
                Order order = new Order()
                {
                    CustomerId = int.Parse(User.FindFirst(ClaimTypes.Sid).Value),
                    BranchId = (int)branchId,
                    CreationDate = DateTime.Now
                };
                _context.Orders.Add(order);
                List<OrderProducts> orderProducts = new List<OrderProducts>();
                foreach (var p in dic.Values)
                {
                    orderProducts.Add(new OrderProducts()
                    {
                        OrderId = order.Id,
                        ProductId = p.Item1.Id,
                        Quantity = p.Item2
                    });
                }

                _context.OrderProducts.AddRange(orderProducts);

                _context.SaveChanges();
                ClearCookies(Request, Response);
                Order orderToPost = _context.Orders.Include(o => o.Branch)
                    .Include(o => o.Customer).FirstOrDefault(o => o.Id == order.Id);
                try
                {
                    new FacebookModel().PostPost($"{orderToPost.Customer.FirstName} " +
                    $"{orderToPost.Customer.LastName} just bought in our amazing " +
                        $"{orderToPost.Branch.Name} branch!");
                }
                catch (WebException)
                { }
                
                return RedirectToAction(nameof(SuccessfullOrder), new { id = order.Id });
            }
            else
            {
                return RedirectToAction(nameof(Index));
            }
        }


        public IActionResult SuccessfullOrder(int? id)
        {
            return View(_context.Orders.Include(order => order.Branch).FirstOrDefault(order => order.Id == id));
        }
    }
}

﻿using System;
using System.IO;
using System.Net;
using Newtonsoft.Json.Linq;

namespace SuperDuperMarket.Models
{
    public class GeoLocationModel
    {

        public String Url
        {
            get;
            private set;
        }

        public String ApiKey
        {
            get;
            private set;
        }

        public static GeoLocationModel()
        {
            // TODO We will need to change this url and api key to a conf file
            this.Url = "https://api.opencagedata.com/geocode/v1/json/";
            this.ApiKey = "ef1da18326ef4a7cacf0a31f72dfbb1f";
        }



        public static string GetGeoLocationByAddress(string address)
        {
            HttpWebRequest req = (HttpWebRequest)WebRequest.
                Create($"{Url}q={address}&key={ApiKey}");
            HttpWebResponse res = (HttpWebResponse)req.GetResponse();
            Stream stream = res.GetResponseStream();
            StreamReader reader = new StreamReader(stream);
            string resInString = reader.ReadToEnd();
            stream.Close();
            reader.Close();
            JObject json = JObject.Parse(resInString);
            return (json["results"][0]["geometry"]).ToString();
        }
    }
}

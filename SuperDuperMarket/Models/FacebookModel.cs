﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace SuperDuperMarket.Models
{
    public class FacebookModel : HttpHandlerModel
    {
        public readonly string AppAccessToken;
        public readonly string PageAccessToken;
        public readonly string AppID;
        public FacebookModel():base("https://graph.facebook.com/me/", "EAALsGmjKDUEBAJkmLiUFIIWAVCQZBpWOPuPG90W4vcRWZBzu1WBwqeBZAA9ZBAQHp946XbeFRytVWGG0AWFlw40HLIgqfBCbnaX2klFSNTZAm4JcwLVzqClqqJ8jJ259LQ5K7KZAI8AR7yCHHRAgCN1ialkt4zajMGyQNZA23vrckVrsuPEZA2Nb")
        {
            AppID = "822548124798273";
            AppAccessToken = "822548124798273|UwX8qZne1FXrWsATFKWfZ6s1XvU";
        }

        public string GetPosts()
        {
            string UrlParams = $"access_token={ApiKey}";
            string Path = "posts";
            JObject json = Get(UrlParams, Path);
            return json.ToString();
        }

        public string PostPost(string message)
        {
            string UrlParams = $"message={message}&access_token={ApiKey}";
            string Path = "feed";
            JObject json = Post(UrlParams, Path);
            return json.ToString();
        }
    }
}

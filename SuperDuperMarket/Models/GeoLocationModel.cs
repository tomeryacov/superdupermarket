﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace SuperDuperMarket.Models
{
    public class  GeoLocationModel : HttpHandlerModel
    {
        public GeoLocationModel():base("https://api.opencagedata.com/geocode/v1/json/", "ef1da18326ef4a7cacf0a31f72dfbb1f")
        {
            // TODO We will need to change this url and api key to a conf file
        }

        public Dictionary<string,object> GetGeoLocationByAddress(string street, string city)
        {
            string address = $"{street}, {city}, Israel";
            string UrlParams = $"q={address}&key={ApiKey}";
            JObject json = Get(UrlParams);
            return JObject.Parse(json["results"][0]["geometry"].ToString()).ToObject<Dictionary<string, object>>();
        }
    }

    public class Location
    {
        public float lat { get; private set; }
        public float lng { get; private set; }

        public Location(float lat, float lng)
        {
            this.lat = lat;
            this.lng = lng;
        }

        public override string ToString()
        {
            return JsonConvert.SerializeObject(this);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SuperDuperMarket.Models
{
    public class ProductsViewModel
    {
        public List<Product> Products;
        public List<Product> Recommended;
        public string RecommenededCategory;

        public ProductsViewModel(List<Product> products, List<Product> recommended, string recommenededCategory)
        {
            Products = products;
            Recommended = recommended;
            RecommenededCategory = recommenededCategory;
        }
    }
}

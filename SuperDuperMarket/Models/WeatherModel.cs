﻿using System;
using System.IO;
using System.Net;
using Newtonsoft.Json.Linq;

namespace SuperDuperMarket.Models
{
    public class WeatherModel : HttpHandlerModel
    {
        public WeatherModel(): base("http://api.openweathermap.org/data/2.5/weather/","57b5c13436da25e9f451f2b427a0fc25")
        {
            // TODO We will need to change this url and api key to a conf file
        }


        /// <summary>
        /// Gets the weather by geo
        /// </summary>
        /// <returns>The weather by geo.</returns>
        /// <param name="lon">Lon.</param>
        /// <param name="lat">Lat.</param>
        public double GetWeatherByGeo(double lon, double lat)
        {
            string UrlParams = $"lat={lat}&lon={lon}&units=metric&appid={ApiKey}";
            JObject json = Get(UrlParams);
            return (double.Parse((json["main"]["temp"]).ToString()));
        }
    }
}

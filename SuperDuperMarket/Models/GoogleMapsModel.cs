﻿using Microsoft.AspNetCore.Mvc.ViewFeatures;
using Newtonsoft.Json;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace SuperDuperMarket.Models
{
    public class GoogleMapsModel : HttpHandlerModel
    {
        public GoogleMapsModel() : base("", "AIzaSyBK7l1lWTrHl-O4ycTnE8bDiF_8Jzsb7Gs")
        {
            Url = $"https://maps.googleapis.com/maps/api/js?key={ApiKey}&callback=initMap";
        }

        public string GetUrl() => string.Format(this.Url, this.ApiKey);

        public string BranchesInformation(List<Branch> branches)
        {
            ConcurrentBag<Dictionary<string, object>> locations = new ConcurrentBag<Dictionary<string, object>>();
            List<Thread> threads = new List<Thread>();
            GeoLocationModel geoLocationModel = new GeoLocationModel();
            foreach (var branch in branches)
            {
                // In a new thread, get the geolocation of the branch
                Thread t = new Thread(() => {
                    Dictionary<string, object> location = geoLocationModel.GetGeoLocationByAddress(branch.Address, branch.City);
                    location.Add("info", branch.Name);
                    location.Add("address", $"{branch.Address}, {branch.City}");
                    location.Add("id", branch.Id);
                    locations.Add(location);
                });
                threads.Add(t);
                t.Start();
            }

            // Wait for all lookups to finish
            foreach (Thread thread in threads)
                thread.Join();
            return JsonConvert.SerializeObject(locations);
        }

        public void ShowOnMap(List<Branch> branches, ViewDataDictionary ViewData)
        {
            ViewData["mapsUrl"] = this.Url;
            ViewData["branches"] = this.BranchesInformation(branches);
        }
    }
}

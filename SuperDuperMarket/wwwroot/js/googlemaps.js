﻿function initBranchesMap(locations) {
    var map = new google.maps.Map(
        document.getElementById('map'), { zoom: 10, center: locations[0] });

    for (var i = 0; i < locations.length; i++) {
        // This shit is a closure for creating multiple markers with infowindows
        // Don't touch it, it works :)
        var marker = new google.maps.Marker({ position: locations[i], map: map, title: locations[i]['info'] });
        var content =
            "<div style='padding:15px'>" +
            `<h5>${locations[i]['info']}</h5>` +
            `<p> Open 24/7 </p>` +
            `<h6>${locations[i]['address']}</6>` +
            "</div>"
        var infowindow = new google.maps.InfoWindow({});
        google.maps.event.addListener(marker, 'click', (function (marker, content, infowindow) {
            return function () {
                infowindow.setContent(content);
                infowindow.open(map, marker);
            };
        })(marker, content, infowindow));
    }

}
﻿using System;
using SuperDuperMarket.Models;
using System.Linq;
using System.Collections.Generic;

namespace SuperDuperMarket.Data
{
    public static class DbInitializer
    {
        public static void Initialize(SDMDbContext context)
        {
            context.Database.EnsureCreated();

            // Look for any customers.
            if (context.Customers.Any())
            {
                return;   // DB has been seeded
            }

            context.Customers.AddRange(
                    new List<Customer>() {
                        new Customer
                        {
                            //Id = 1,
                            FirstName = "Admin",
                            LastName = "Admin",
                            BirthDate = new DateTime(1995, 11, 04),
                            Email = "admin@sdm.com",
                            Password = "admin",
                            Address = "admin",
                            City = "admin",
                            ZipCode = 1,
                            IsAdmin = true
                        },
                         new Customer
                         {
                            // Id = 2,
                             FirstName = "Tomer",
                             LastName = "Yacov",
                             BirthDate = new DateTime(1996, 3, 12),
                             Email = "tomeryacov@gmail.com",
                             Password = "tomeryacov",
                             Address = "hailanot 73",
                             City = "Hertzliya",
                             ZipCode = 4510301,
                             IsAdmin = false
                         },
                         new Customer
                         {
                             //Id = 3,
                             FirstName = "Sapir",
                             LastName = "Mosacho",
                             BirthDate = new DateTime(1997, 7, 16),
                             Email = "sapman@gmail.com",
                             Password = "160797",
                             Address = "18 Laskov",
                             City = "Holon",
                             ZipCode = 4510301,
                             IsAdmin = false
                         }
                     }
                );
            context.Orders.AddRange(
                    new List<Order>() {
                        new Order
                        {
                            CustomerId = 2,
                            BranchId = 1,
                            CreationDate = DateTime.Now.AddDays(-1).AddHours(-2)
                        },
                        new Order
                        {
                            CustomerId = 2,
                            BranchId = 1,
                            CreationDate = DateTime.Now.AddDays(-2)
                        },
                        new Order
                        {
                            CustomerId = 3,
                            BranchId = 2,
                            CreationDate = DateTime.Now.AddDays(-1)
                        },
                        new Order
                        {
                            CustomerId = 3,
                            BranchId = 5,
                            CreationDate = DateTime.Now.AddHours(-2).AddHours(-1)
                        },
                         new Order
                         {
                            CustomerId = 2,
                            BranchId = 3,
                            CreationDate = DateTime.Now.AddDays(-1).AddHours(-2)
                         }
                     }
                );

            context.Products.AddRange(new List<Product>() {
                        new Product
                        {
                           // Id = 1,
                            Name = "Milk 3% fat",
                            Description = "3 % fat milk from Tnova",
                            ImagePath="~/images/Milk.jpeg",
                            Category="Diary",
                            Price = 6.90
                        },
                         new Product
                         {
                           // Id = 2,
                            Name = "Milky",
                            Description = "Milky, the perfect combination of chocolate and whipped cream",
                            ImagePath="~/images/Milky.jpeg",
                            Category="Diary",
                            Price = 8
                         },
                          new Product
                         {
                           // Id = 3,
                            Name = "Sabra Hummus",
                            Description = "blend of chickpeas, tahini, and a" +
                                " variety of herbs and spices",
                            ImagePath="~/images/Sabra.jpeg",
                            Category="Legumes",
                            Price = 15.60
                         },
                         new Product
                         {
                            //Id = 4,
                            Name = "Danone",
                            Description = "Danone yogurt, in a variety of" +
                                " wonderful tastes, gives your body energy, " +
                                "strength, and strong bones",
                            ImagePath="~/images/Danone.jpeg",
                            Category="Diary",
                            Price = 5.30
                         },
                         new Product
                         {
                            //Id = 5,
                            Name = "Asado",
                            Description = "Asado steak",
                            ImagePath="~/images/Asado.jpeg",
                            Category="Meat",
                            Price = 80
                         },
                         new Product
                         {
                            //Id = 6,
                            Name = "Hotdogs",
                            Description = "Frozen hotdogs",
                            ImagePath="~/images/Hotdogs.jpeg",
                            Category="Meat",
                            Price = 21.60
                         },
                         new Product
                         {
                            Name = "Tomato",
                            Description = "Red Tomato",
                            ImagePath="~/images/Tomato.jpeg",
                            Category="Veg",
                            Price = 4
                            },
                         new Product
                            {
                              Name = "Cucamber",
                              Description = "Green Cucamber",
                            ImagePath="~/images/Cucamber.jpeg",
                              Category="Veg",Price = 5
                              },
                        new Product
                        {
                        Name = "Bread",
                           Description = "White bread",
                            ImagePath="~/images/Bread.jpeg",
                           Category="Bread",
                           Price = 6
                           },
                          new Product
                            {
                            Name = "Coca-Cola",
                           Description = "1.5l Coca Cola",
                            ImagePath="~/images/Coca-Cola.jpeg",
                           Category="Drinks",
                           Price = 8
                           },
                        new Product
                            {
                            Name = "Sprite",
                           Description = "1.5l Sprite",
                            ImagePath="~/images/Sprite.jpeg",
                           Category="Drinks",
                           Price = 8
                           },

            });

            context.Branches.AddRange(new List<Branch>()
            {
                new Branch{Name="SuperDuperMarket Herzliya",Address="73 HaIlanot street",City="Herzliya"},
                new Branch{Name="SuperDuperMarket Modiin",Address="Nahal Zohar 13",City="Modiin"},
                new Branch{Name="SuperDuperMarket Holon",Address="Golda Me'ir 87",City="Holon"},
                new Branch{Name="SuperDuperMarket Tel-Aviv",Address="Aven Gvirol 56",City="Tel Aviv"},
                new Branch{Name="SuperDuperMarket Eilat",Address="Eilat Ramon Airport",City="Eilat"}
            });

            context.SaveChanges();

            context.OrderProducts.AddRange(
                    new List<OrderProducts>() {
                        new OrderProducts
                        {
                            OrderId = 2,
                            ProductId = 1,
                            Quantity = 5
                        },
                        new OrderProducts
                        {
                            OrderId = 2,
                            ProductId = 3,
                            Quantity = 5
                        },
                        new OrderProducts
                        {
                            OrderId = 2,
                            ProductId = 4,
                            Quantity = 2
                        },
                        new OrderProducts
                        {
                            OrderId = 3,
                            ProductId = 5,
                            Quantity = 2
                        },
                        new OrderProducts
                        {
                            OrderId = 3,
                            ProductId = 8,
                            Quantity = 1
                        },
                        new OrderProducts
                        {
                            OrderId = 3,
                            ProductId = 2,
                            Quantity = 3
                        },
                        new OrderProducts
                        {
                            OrderId = 4,
                            ProductId = 4,
                            Quantity = 1
                        },
                        new OrderProducts
                        {
                            OrderId = 4,
                            ProductId = 7,
                            Quantity = 5
                        },
                        new OrderProducts
                        {
                            OrderId = 5,
                            ProductId = 9,
                            Quantity = 5
                        },
                        new OrderProducts
                        {
                            OrderId = 5,
                            ProductId = 2,
                            Quantity = 5
                        },

                         new OrderProducts
                         {
                            OrderId = 1,
                            ProductId = 1,
                            Quantity = 3
                         }
                     });


            context.SaveChanges();
        }
    }
}
